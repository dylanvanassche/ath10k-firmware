# Xiaomi Beryllium WiFi ath10k

Hello Kalle,

I would like to include the board file for the Xiaomi POCOPHONE F1 smartphone.
This smartphone is manufactured by Xiaomi and uses the Snapdragon SDM845 SoC.
At postmarketOS, we run the mainline kernel on this phone so shipping this board file
from upstream would be a great addition. A variant property was needed to avoid colliding
with other SDM8845-based smartphones since they all share the same qmi-board-id and qmi-chip-id values.

Below, the questions from the wiki:

* description for what hardware this is:

Smartphone with a SDM845 chip and a WCN3990 Bluetooth/WiFi chip.
The WCN3990 chip is connected to 2 QM488xx radios, one is used
for 2.4Ghz and the other one for 5Ghz radio.

* origin of the board file (did you create it yourself or where you
  downloaded)

Extracted from the stock firmware.

* ids to be used with the board file (ATH10K_BD_IE_BOARD_NAME in ath10k)

- WCN3990 hw1.0
  + bus=snoc,qmi-board-id=ff,qmi-chip-id=30214,variant=xiaomi_beryllium
    sha256sum: 470a670fa15fd1f5625246556c83cb3aa811b0e954813da518e241e3a48bf4a2
    md5sum: 0655a0b7865c13172c43422c035d02a2

* attach the actual board file (board.bin)

The name of the files are equal to the id string in the board-2.bin (minus the ".bin")

Kind regards,
Dylan Van Assche
