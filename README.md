# ath10k WiFi firmware files

SDM845-based devices use the WCN3990 WiFi/Bluetooth chip which stores
its calibration data in `board-2.bin`. This binary file can be generated
from the binary calibration data file and a JSON file containing the matching string.

## Extracting JSON file and calibration data

1. Get the `board-2.bin` file from stock firmware.
2. Get the tool from [qca-swiss-army-knife](https://github.com/qca/qca-swiss-army-knife/blob/master/tools/scripts/ath10k/ath10k-bdencoder) repo.
3. Use `./ath10k-bdencoder -e board-2.bin` to extract the calibration data and the JSON file with the matching string.

## Generating a `board-2.bin`

1. Get the tool from [qca-swiss-army-knife](https://github.com/qca/qca-swiss-army-knife/blob/master/tools/scripts/ath10k/ath10k-bdencoder) repo.
2. Use `./ath10k-bdencoder -c board-2.json` to generate a binary `board-2.bin` file.

## Finding the matching string

1. Flash a mainline-based distro on the device such as [postmarketOS](https://postmarketos.org).
2. Boot it and check `dmesg` output for.

Example:

```
[   10.534431] ath10k_snoc 18800000.wifi: qmi chip_id 0xAAAAA chip_family 0xBBBBB board_id 0xCC soc_id 0xDDDDDDD
```

The values `0xAAAAA`, `0xBBBBB`, `0xCC`, and `0xDDDDDDD' are used for matching by the firmware.
